﻿using UnityEngine;
using System.Collections;

public class StrangeBall : MonoBehaviour {

    private Rigidbody2D rigid;
    public float minForce = 10.0f;
    public float maxForce = 40.0f;
    public CircleCollider2D effector;
    public float force = 5.0f;
    public float maxVelMagnitude = 5.0f;
    public GameController gc;
    private bool flaged = false;
    public int ballsCount = 0;
    private bool freeForceActive = false;
    private float counter = 0;
    private CircleCollider2D selfCollider;
    private Effector effectorCtrl;
    // Use this for initialization

    public void init()
    {
        rigid = GetComponent<Rigidbody2D>();
        selfCollider = GetComponent<CircleCollider2D>();
        //addRandomForce();
        effectorCtrl = effector.GetComponent<Effector>();
        force = effectorCtrl.size;
        rigid.mass = force;
        rigid.velocity = Vector2.zero;
    }


    void Update()
    {
        if(freeForceActive)
        {
            counter += Time.deltaTime;
            if(counter>=0.5f)
            {
                freeForceActive = false;
                selfCollider.enabled = true;
                effector.enabled = true;
            }
        }

       
        if (rigid.velocity.magnitude > maxVelMagnitude)
        { 
            rigid.velocity = rigid.velocity.normalized * maxVelMagnitude;
        }

        Vector2 inScreen = Camera.main.WorldToViewportPoint(transform.position);
        if(inScreen.x<0 || inScreen.x>1.0f || inScreen.y>1.0f || inScreen.y<0.0f)
        {
            rigid.velocity = -(transform.position.normalized) * maxVelMagnitude * 0.5f;
        }

         if(gc.forceAwayActive==true && effectorCtrl.size!=1)
        {
            effectorCtrl.resetSize();
        }
    }


    public void moveTo(StrangeBall sb)
    {
        Vector2 tempForce;
        tempForce = this.transform.position - sb.transform.position;
        tempForce.Normalize();
        if (gc.forceAwayActive)
        {
            sb.addForce(tempForce * -force * 5.0f); ;
        }
        else
        {
            float dist = Vector3.Distance(this.transform.position, sb.transform.position);
            float mul = dist / effectorCtrl.size;
            sb.addForce(tempForce * ((force*sb.force)/(dist*dist)) );
        }
    }


    void addForce(Vector2 f)
    {
        rigid.AddForce(f);
    }
        

    void addEscapeForce(Vector3 initPos)
    {
        Vector2 dir = transform.position - initPos;
        dir.Normalize();

        float force = Random.Range(minForce, maxForce);
        rigid.AddForce(dir * force*Random.Range(0.0f,100.0f),ForceMode2D.Impulse);
     
    }

    public void addFreeForce(Vector2 initPos)
    {
        freeForceActive = true;
        selfCollider.enabled = false;
        effector.enabled = false;
        addEscapeForce(initPos);
    }


    void OnCollisionEnter2D(Collision2D coll)
    {
        if (gc.forceAwayActive == true) return;
        flaged = true;
        StrangeBall sb = coll.transform.GetComponent<StrangeBall>();
        if (sb.force < force || sb.flaged == false)
        {
            force += sb.force;
            rigid.mass = force;
            effectorCtrl.addSize(sb.effectorCtrl.size);
            Vector3 temp = transform.localScale;
            temp.x += sb.transform.localScale.x;
            temp.y += sb.transform.localScale.y;
            transform.localScale = temp;
            ballsCount += sb.ballsCount;
            Destroy(sb.gameObject);  
            ballsCount++;
            if(ballsCount>=50)
            {   
                gameObject.SetActive(false);
                gc.fiftyCreate(this.transform.position);
            }
        }
    }


}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject strangeBallPrefab;
    public float minSpawnDist;
    public float maxSpawnDist;
    public Text ballsNumberText;
    public float timeToSpawn;
    private float counter;
    private int ballsCounter;
    private bool spawn = true;

    public float growScaleMul = 1.05f;
    public float growForceMul = 1.05f;
    public float growRadiusMul = 1.05f;

    public bool forceAwayActive = false;
	// Use this for initialization
	void Start () {
        Physics2D.gravity = new Vector2(0, 0);
        ballsNumberText.text = "";
        forceAwayActive = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (spawn)
        {
            counter += Time.deltaTime;
            if (counter > timeToSpawn)
            {
                spawnInitBall();
                counter = 0;
                ballsCounter++;
                if(ballsCounter>=250)
                {
                    spawn = false;
                    forceAwayActive = true;
                }
            }
            ballsNumberText.text = ballsCounter.ToString();
        }


	}

    public void fiftyCreate(Vector2 initPos)
    {
        for(int a=0;a<50;a++)
        {
            ballsCounter++;
            ballsNumberText.text = ballsCounter.ToString();
            if (ballsCounter >= 250)
            {
                spawn = false;
                forceAwayActive = true;
                return;
            }
            else spawnBall(initPos);
        }
    }

    Vector2 getRandomPos()
    {
        float dist = Random.Range(minSpawnDist, maxSpawnDist);
        Vector2 dir = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        dir.Normalize();
        return dir * dist;
    }

    Vector2 getRandomPosOnScreen()
    {
        Vector2 view = new Vector2(Random.Range(0, 1.0f), Random.Range(0, 1.0f));
        return Camera.main.ViewportToWorldPoint(view);
    }

    void spawnBall(Vector2 offset)
    {
        GameObject obj = Instantiate(strangeBallPrefab, getRandomPos()+offset,Quaternion.identity) as GameObject;
        StrangeBall sb = obj.GetComponent<StrangeBall>();
        sb.init();
        sb.gc = this;
        if (offset != Vector2.zero)
        {
            sb.addFreeForce(offset);
        }
    }

    void spawnInitBall()
    {
        GameObject obj = Instantiate(strangeBallPrefab, getRandomPosOnScreen(), Quaternion.identity) as GameObject;
        StrangeBall sb = obj.GetComponent<StrangeBall>();
        sb.init();
        sb.gc = this;
    }
}

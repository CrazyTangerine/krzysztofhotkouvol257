﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Effector : MonoBehaviour {

    public StrangeBall controller;
    public float size = 9.0f;
    private CircleCollider2D col;
    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        col.radius = size;
    }
    void OnTriggerStay2D(Collider2D col)
    {
        Effector ef=col.transform.GetComponent<Effector>();
        if(ef!=null)
        {
            if(ef.controller != controller)
            {
                ef.controller.moveTo(controller);
            }
        }
    }

    public void addSize(float sizeToAdd)
    {
        size += sizeToAdd;
        col.radius = size;
    }

    public void resetSize()
    {
        size =1;
        col.radius = 1;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Slingshot : MonoBehaviour {

    public CameraFollow followBall;
    public GameControlBall controller;
    public float minStretch;
    public float maxStretch;
    public LineRenderer lineRender;
    public Transform lineRendererStart;

    private Vector3[] tempArray;
    private SpringJoint2D springElem;
    private Rigidbody2D rigid;
    private bool canDrag = false;
    private Vector2 oldVelocity;
    private Ball ball;
    private bool checkingBall = false;
	void Start () {
        tempArray = new Vector3[2];
        setLineRenderer(false);
	}

    public void setBall(Ball b)
    {
        springElem = b.getJoint();
        rigid = b.getRigid();
        ball = b;
    }

    IEnumerator checkBall()
    {
        while (checkingBall)
        {
            yield return new WaitForSeconds(1);
            if (ball.velLowEnough())
            {
                checkingBall = false;
                followBall.resetCam();
                controller.newBall();
            }
        }
    }
	void Update () {
      
        if (ball == null) return;

        if(canDrag)
        {
            DoDrag();
        }

        if(springElem!=null)
        {
            if(rigid.isKinematic==false && oldVelocity.sqrMagnitude>rigid.velocity.sqrMagnitude)
            {
                Destroy(springElem);
                rigid.velocity = oldVelocity;
            }

            setLineRenderer(false);

            if(canDrag==false)
            {
                oldVelocity = rigid.velocity;
            }

        }
        else
        {
            setLineRenderer(true);
        }

        if (checkingBall == false)
        {

            if (Input.GetMouseButtonDown(0))
            {

                springElem.enabled = false;
                canDrag = true;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                springElem.enabled = true;
                rigid.isKinematic = false;
                canDrag = false;

                //follow ball
                followBall.IsFollowing = true;
                followBall.toFollow = ball.transform;
                checkingBall = true;
                StartCoroutine(checkBall());
            }
        }
	}

    private void setLineRenderer(bool empty)
    {
        if (empty)
        {
            tempArray[0] = tempArray[1] = Vector2.zero;
        }
        else
        {
            tempArray[0] = lineRendererStart.position;
            tempArray[1] = ball!=null?ball.transform.position:transform.position;
        }
        lineRender.SetPositions(tempArray);
    }

    void DoDrag()
    {
        Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mPos.z = 0;

        Vector2 dirSling = mPos-lineRendererStart.position;
        Vector2 dir = dirSling.normalized;

        Vector2 targetPos = new Vector2(lineRendererStart.position.x, lineRendererStart.position.y) + dir * Mathf.Clamp(dirSling.magnitude, minStretch, maxStretch);

        ball.transform.position = targetPos;
    }

}

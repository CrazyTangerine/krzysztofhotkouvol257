﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform toFollow;
    private Transform cam;
    private Vector3 posTemp = Vector3.zero;
    private Vector3 startPos;
    public bool IsFollowing
    {
        get;set;
    }
    void Start()
    {
        cam = GetComponent<Transform>();
        startPos = transform.position;
    }

    public void resetCam()
    {
        transform.position = startPos;
        IsFollowing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(IsFollowing)
        {
            posTemp = cam.position;
            posTemp.x = toFollow.position.x;
            cam.position = Vector3.Lerp(cam.position, posTemp, Time.deltaTime);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour {

    public float velLimit = 1.0f;
    private SpringJoint2D springElem;
    private Rigidbody2D rigid;
	// Use this for initialization
	void Awake () {
        springElem = GetComponent<SpringJoint2D>();
        rigid = GetComponent<Rigidbody2D>();
	}

    public SpringJoint2D getJoint()
    {
        return springElem;
    }

    public Rigidbody2D getRigid()
    {
        return rigid;
    }

    public bool velLowEnough()
    {
        return rigid.velocity.magnitude < velLimit;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControlBall : MonoBehaviour
{
    public Slingshot slingControl;
    public GameObject ballPrefab;
    // Start is called before the first frame update
    void Start()
    {
        createAndSetBall();
    }

    private void createAndSetBall()
    {
        GameObject o = GameObject.Instantiate(ballPrefab);
        o.SetActive(true);
        Ball b = o.GetComponent<Ball>();
        slingControl.setBall(b);
    }

    public void newBall()
    {
        createAndSetBall();
    }

}
